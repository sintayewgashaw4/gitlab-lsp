import { Snowplow } from '../../common/tracking/snowplow/snowplow';
import { Fetch } from '../../node/fetch';

const structEvent = {
  category: 'test',
  action: 'test',
  label: 'test',
  value: 1,
};

const enabledMock = jest.fn();

describe('Snowplow', () => {
  describe('when hostname cannot be resolved', () => {
    it('should disable sending events', async () => {
      const lsFetch = new Fetch();
      await lsFetch.initialize();
      const logSpy = jest.spyOn(global.console, 'log');
      const sp = Snowplow.getInstance(lsFetch, {
        appId: 'test',
        timeInterval: 1000,
        maxItems: 2,
        endpoint: 'http://askdalksdjalksjd.asdasdasd',
        enabled: enabledMock,
      });

      enabledMock.mockReturnValue(true);

      expect(sp.disabled).toBe(false);

      await sp.trackStructEvent(structEvent);
      await sp.stop();

      expect(sp.disabled).toBe(true);
      expect(logSpy).toHaveBeenCalledWith(
        'Disabling telemetry, unable to resolve endpoint address.',
      );
      logSpy.mockClear();

      await sp.trackStructEvent(structEvent);
      await sp.stop();

      expect(sp.disabled).toBe(true);
      expect(logSpy).not.toHaveBeenCalled();
    });
  });
});
