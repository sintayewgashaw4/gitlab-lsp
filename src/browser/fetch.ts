import { FetchBase, IFetch } from '../common/fetch';

/**
 * This browser implementation doesn't do anything
 * as proxy handling should be automatic.
 */
export class Fetch extends FetchBase implements IFetch {}
