import fetch from 'cross-fetch';
import { Fetch } from './fetch';
import { IFetch } from '../common/fetch';

jest.mock('cross-fetch');

describe('LsFetch', () => {
  afterEach(() => {
    (fetch as jest.Mock).mockClear();
  });

  describe('fetch', () => {
    describe('not initialized', () => {
      it('should throw Error', async () => {
        const lsFetch = new Fetch();

        await expect(lsFetch.fetch('https://gitlab.com/')).rejects.toThrowError(
          'LsFetch not initialized. Make sure LsFetch.initialized() was called.',
        );
      });
    });

    describe('request Methods', () => {
      const testCases: [keyof IFetch, string][] = [
        ['get', 'GET'],
        ['post', 'POST'],
        ['put', 'PUT'],
        ['delete', 'DELETE'],
      ];

      test.each(testCases)(
        'should call cross-fetch fetch for %s',
        async (method, expectedMethod) => {
          const lsFetch = new Fetch();
          await lsFetch.initialize();

          await lsFetch[method]('https://gitlab.com/');
          expect(fetch).toHaveBeenCalled();

          const [url, init] = (fetch as jest.Mock).mock.calls[0];
          expect(init.method).toBe(expectedMethod);
          expect(url).toBe('https://gitlab.com/');
        },
      );
    });
  });
});
