import { ProposedFeatures, TextDocuments, createConnection } from 'vscode-languageserver/node';
import { TextDocument } from 'vscode-languageserver-textdocument';
import { ConfigProvider, DocumentTransformer, setup } from '../common';
import { Console } from 'node:console';
import { Fetch } from './fetch';
import { SecretRedactor } from '../common/secret_redaction';

const configProvider = new ConfigProvider();
const connection = createConnection(ProposedFeatures.all);

// Send all console messages to stderr. Stdin/stdout may be in use
// as the LSP communications channel.
//
// This has to happen after the `createConnection` because the `vscode-languageserver` server version 9 and higher
// patches the console as well
// https://github.com/microsoft/vscode-languageserver-node/blob/84285312d8d9f22ee0042e709db114e5415dbdde/server/src/node/main.ts#L270
//
// FIXME: we should really use the remote logging with `window/logMessage` messages
// That's what the authors of the languageserver-node want us to use
// https://github.com/microsoft/vscode-languageserver-node/blob/4e057d5d6109eb3fcb075d0f99456f05910fda44/server/src/common/server.ts#L133
global.console = new Console({ stdout: process.stderr, stderr: process.stderr });

const documents: TextDocuments<TextDocument> = new TextDocuments(TextDocument);
const lsFetch = new Fetch();
const transformer = new DocumentTransformer(documents, [new SecretRedactor(configProvider)]);

console.log('GitLab Language Server is starting');

// FIXME: this eslint violation was introduced before we set up linting
// eslint-disable-next-line @typescript-eslint/no-floating-promises
lsFetch.initialize().then(() => {
  setup(connection, transformer, lsFetch, configProvider);

  // Make the text document manager listen on the connection for open, change and close text document events
  documents.listen(connection);
  // Listen on the connection
  connection.listen();
  console.log('GitLab Language Server has started');
});
