import fetch from 'cross-fetch';
import { getProxySettings } from 'get-proxy-settings';
import { ProxyAgent } from 'proxy-agent';
import { IFetch, FetchBase } from '../common/fetch';

export interface LsRequestInit extends RequestInit {
  agent?: ProxyAgent;
}

/**
 * Wrap fetch to support proxy configurations
 */
export class Fetch extends FetchBase implements IFetch {
  #proxy?: ProxyAgent;
  #initialized: boolean = false;
  #userProxy?: string;

  constructor(userProxy?: string) {
    super();

    this.#userProxy = userProxy;
  }

  async initialize(): Promise<void> {
    const proxy = await getProxySettings();

    // Set http_proxy and https_proxy environment variables
    // which will then get picked up by the ProxyAgent and
    // used.

    if (proxy?.http) {
      process.env.http_proxy = `${proxy.http.protocol}://${proxy.http.host}:${proxy.http.port}`;
    }
    if (proxy?.https) {
      process.env.https_proxy = `${proxy.https.protocol}://${proxy.https.host}:${proxy.https.port}`;
    }
    if (this.#userProxy) {
      process.env.http_proxy = this.#userProxy;
      process.env.https_proxy = this.#userProxy;
    }

    if (process.env.http_proxy || process.env.https_proxy) {
      this.#proxy = new ProxyAgent();
      console.log(`Setting proxy to '${process.env.https_proxy || process.env.http_proxy}'`);
    }

    this.#initialized = true;
  }

  async destroy(): Promise<void> {
    this.#proxy?.destroy();
  }

  async fetch(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    if (!this.#initialized) {
      throw new Error('LsFetch not initialized. Make sure LsFetch.initialized() was called.');
    }

    if (this.#proxy) {
      if (!init) {
        init = {};
      }

      (init as LsRequestInit).agent = this.#proxy;
    }

    return fetch(input, init);
  }
}
