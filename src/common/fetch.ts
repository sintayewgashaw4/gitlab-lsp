import fetch from 'cross-fetch';

export interface IFetch {
  initialize(): Promise<void>;
  destroy(): Promise<void>;
  fetch(input: RequestInfo | URL, init?: RequestInit): Promise<Response>;
  delete(input: RequestInfo | URL, init?: RequestInit): Promise<Response>;
  get(input: RequestInfo | URL, init?: RequestInit): Promise<Response>;
  post(input: RequestInfo | URL, init?: RequestInit): Promise<Response>;
  put(input: RequestInfo | URL, init?: RequestInit): Promise<Response>;
}

export class FetchBase {
  updateRequestInit(method: string, init?: RequestInit): RequestInit {
    if (typeof init === 'undefined') {
      init = { method: method };
    } else {
      init.method = method;
    }

    return init;
  }

  async initialize(): Promise<void> {}
  async destroy(): Promise<void> {}

  async fetch(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    return fetch(input, init);
  }

  async delete(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    return this.fetch(input, this.updateRequestInit('DELETE', init));
  }

  async get(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    return this.fetch(input, this.updateRequestInit('GET', init));
  }

  async post(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    return this.fetch(input, this.updateRequestInit('POST', init));
  }

  async put(input: RequestInfo | URL, init?: RequestInit): Promise<Response> {
    return this.fetch(input, this.updateRequestInit('PUT', init));
  }
}
