import { isAtOrNearEndOfLine } from './suggestion_filter';

describe('end of line or only special characters remaining', () => {
  it.each`
    name                                           | suffix                           | expected
    ${'is at end of line'}                         | ${'\nconsole.log("hello")'}      | ${true}
    ${'is at end of file'}                         | ${''}                            | ${true}
    ${'only has whitespace after current line'}    | ${'   '}                         | ${true}
    ${'only has special chars after current line'} | ${'");\nconsole.log("hello")\n'} | ${true}
    ${'precedes a unix line ending'}               | ${'\nconsole.log("hello")'}      | ${true}
    ${'precedes a windows line ending'}            | ${'\r\nconsole.log("hello")'}    | ${true}
    ${'is mid-word'}                               | ${'ole.log("hello")\n'}          | ${false}
    ${'precedes an escaped line ending'}           | ${'\\n'}                         | ${false}
  `(
    'expect isAtOrNearEndOfLine to be $expected when cursor $name',
    async ({ suffix, expected }) => {
      expect(isAtOrNearEndOfLine(suffix)).toBe(expected);
    },
  );
});
