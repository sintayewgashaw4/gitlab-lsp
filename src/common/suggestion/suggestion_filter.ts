/** Check if we are at the end of the line or if the cursor is followed only by allowed characters */
export function isAtOrNearEndOfLine(suffix: string) {
  const match = suffix.match(/\r?\n/);
  const lineSuffix = match ? suffix.substring(0, match.index) : suffix;
  // Remainder of line only contains characters within the allowed set
  // The reasoning for this regex is documented here:
  // https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/-/merge_requests/61#note_1608564289
  const allowedCharactersPastCursorRegExp = /^\s*[)}\]"'`]*\s*[:{;,]?\s*$/;
  return allowedCharactersPastCursorRegExp.test(lineSuffix);
}
