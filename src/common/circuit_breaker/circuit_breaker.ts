import EventEmitter from 'events';
import { Disposable } from 'vscode-languageserver';
export const CIRCUIT_BREAK_INTERVAL_MS = 10000;
export const MAX_ERRORS_BEFORE_CIRCUIT_BREAK = 4;
export const API_ERROR_NOTIFICATION = '$/gitlab/api/error';
export const API_RECOVERY_NOTIFICATION = '$/gitlab/api/recovered';

enum CircuitBreakerState {
  OPEN = 'Open',
  CLOSED = 'Closed',
}

export class CircuitBreaker {
  private state: CircuitBreakerState = CircuitBreakerState.CLOSED;
  private readonly maxErrorsBeforeBreaking: number;

  private readonly breakTimeMs: number;

  private errorCount = 0;

  #eventEmitter = new EventEmitter();

  constructor(
    maxErrorsBeforeBreaking: number = MAX_ERRORS_BEFORE_CIRCUIT_BREAK,
    breakTimeMs: number = CIRCUIT_BREAK_INTERVAL_MS,
  ) {
    this.maxErrorsBeforeBreaking = maxErrorsBeforeBreaking;
    this.breakTimeMs = breakTimeMs;
  }

  error() {
    this.errorCount += 1;
    if (this.errorCount >= this.maxErrorsBeforeBreaking) {
      this.open();
    }
  }

  open() {
    this.state = CircuitBreakerState.OPEN;
    console.warn(
      `Excess errors detected, opening circuit breaker for ${this.breakTimeMs / 1000} second(s).`,
    );
    setTimeout(() => this.close(), this.breakTimeMs);
    this.#eventEmitter.emit('open');
  }

  isOpen() {
    return this.state === CircuitBreakerState.OPEN;
  }

  close() {
    this.state = CircuitBreakerState.CLOSED;

    if (this.errorCount > 0) {
      this.errorCount = 0;
      this.#eventEmitter.emit('close');
    }
  }

  onOpen(listener: () => void): Disposable {
    this.#eventEmitter.on('open', listener);
    return { dispose: () => this.#eventEmitter.removeListener('open', listener) };
  }

  onClose(listener: () => void): Disposable {
    this.#eventEmitter.on('close', listener);
    return { dispose: () => this.#eventEmitter.removeListener('close', listener) };
  }
}
