import fetch from 'cross-fetch';
import { GitLabAPI, CodeSuggestionResponse } from './api';
import {
  CODE_SUGGESTIONS_RESPONSE,
  CODES_SUGGESTIONS_COMPLETION_TOKEN,
  FILE_INFO,
} from './test_utils/mocks';
import { FetchBase, IFetch } from './fetch';

jest.mock('cross-fetch');

describe('GitLabAPI', () => {
  let gitLabAPI: GitLabAPI;
  const lsFetch: IFetch = new FetchBase();
  const token = 'glpat-1234';
  const nonGitLabBaseUrl = 'http://test.com';
  const gitlabBaseUrl = 'https://gitlab.com';
  const clientInfo = { name: 'MyClient', version: '1.0.0' };

  afterEach(() => {
    (fetch as jest.Mock).mockClear();
  });

  describe('getCodeSuggestions', () => {
    describe('Version1', () => {
      describe('Error path', () => {
        beforeEach(() => {
          gitLabAPI = new GitLabAPI(lsFetch, nonGitLabBaseUrl, undefined);
        });

        it('should throw an error when no token provided', async () => {
          try {
            await gitLabAPI.getCodeSuggestions(FILE_INFO);
          } catch (error) {
            expect(error).toBe('Token needs to be provided');
          }
        });
      });

      describe('Success path', () => {
        let response: CodeSuggestionResponse | undefined;

        beforeEach(async () => {
          gitLabAPI = new GitLabAPI(lsFetch, nonGitLabBaseUrl, token);
          gitLabAPI.setClientInfo({ name: clientInfo.name, version: clientInfo.version });

          (fetch as jest.Mock)
            .mockResolvedValueOnce({
              json: () => CODES_SUGGESTIONS_COMPLETION_TOKEN,
            })
            .mockResolvedValueOnce({
              json: () => CODE_SUGGESTIONS_RESPONSE,
              status: 200,
            });

          response = await gitLabAPI.getCodeSuggestions(FILE_INFO);
        });

        it('should make a request for the access token', () => {
          const [url, params] = (fetch as jest.Mock).mock.calls[0];

          expect(url).toBe(`${nonGitLabBaseUrl}/api/v4/code_suggestions/tokens`);

          expect(params.headers).toMatchObject({
            Authorization: `Bearer ${token}`,
            'User-Agent': `code-completions-language-server-experiment (${clientInfo.name}:${clientInfo.version})`,
          });
        });

        it('should make a request for the code suggestions', () => {
          const [url, params] = (fetch as jest.Mock).mock.calls[1];

          expect(url).toBe('https://codesuggestions.gitlab.com/v2/completions');

          expect(params.headers).toMatchObject({
            Authorization: `Bearer ${CODES_SUGGESTIONS_COMPLETION_TOKEN.access_token}`,
            'Content-Type': 'application/json',
            'X-Gitlab-Authentication-Type': 'oidc',
            'User-Agent': `code-completions-language-server-experiment (${clientInfo.name}:${clientInfo?.version})`,
          });
        });

        it('should return code suggestions', async () => {
          expect(response).toEqual({ ...CODE_SUGGESTIONS_RESPONSE, status: 200 });
        });
      });
    });

    describe('Version2', () => {
      const api = new GitLabAPI(lsFetch, gitlabBaseUrl, token);

      describe('Error path', () => {
        beforeEach(async () => {
          await api.configureApi({ baseUrl: gitlabBaseUrl, token: '' });
        });

        it('should throw an error when no token provided', async () => {
          try {
            await api.getCodeSuggestions(FILE_INFO);
          } catch (error) {
            expect(error).toBe('Token needs to be provided');
          }
        });
      });

      describe('Success path', () => {
        let response: CodeSuggestionResponse | undefined;

        beforeEach(async () => {
          api.setClientInfo({ name: clientInfo.name, version: clientInfo.version });
          await api.configureApi({ baseUrl: gitlabBaseUrl, token });
          (fetch as jest.Mock).mockResolvedValueOnce({
            json: () => CODE_SUGGESTIONS_RESPONSE,
            status: 200,
          });

          response = await api.getCodeSuggestions(FILE_INFO);
        });

        it('should make a request for the code suggestions', () => {
          const [url, params] = (fetch as jest.Mock).mock.calls[0];

          expect(url).toBe(`${gitlabBaseUrl}/api/v4/code_suggestions/completions`);

          expect(params.headers).toMatchObject({
            Authorization: `Bearer ${token}`,
            'User-Agent': `code-completions-language-server-experiment (${clientInfo.name}:${clientInfo?.version})`,
            'Content-Type': 'application/json',
          });
        });

        it('should return code suggestions', async () => {
          expect(response).toEqual({ ...CODE_SUGGESTIONS_RESPONSE, status: 200 });
        });
      });
    });
  });

  describe('configureApi', () => {
    describe('Setting API version', () => {
      beforeEach(() => {
        gitLabAPI = new GitLabAPI(lsFetch, nonGitLabBaseUrl, token);
        gitLabAPI.setClientInfo({ name: clientInfo.name, version: clientInfo.version });
      });

      it('should NOT make a request to check the instance version when on `gitlab.com`', async () => {
        await gitLabAPI.configureApi({ baseUrl: gitlabBaseUrl, token });
        expect(fetch).not.toHaveBeenCalled();
      });

      it('should make a request to check the instance version otherwise', async () => {
        (fetch as jest.Mock).mockResolvedValueOnce({
          json: () => ({ version: '13.0.0-pre' }),
        });

        await gitLabAPI.configureApi({ baseUrl: nonGitLabBaseUrl, token });
        expect(fetch).toHaveBeenCalledWith(`${nonGitLabBaseUrl}/api/v4/version`, {
          headers: {
            Authorization: `Bearer ${token}`,
            'User-Agent': `code-completions-language-server-experiment (${clientInfo.name}:${clientInfo?.version})`,
          },
          method: 'GET',
        });
      });
    });
  });

  describe('checkToken', () => {
    describe('PAT Token', () => {
      const PAT = 'glpat-abcdefghijklmno12345';

      beforeEach(() => {
        gitLabAPI = new GitLabAPI(lsFetch, gitlabBaseUrl, PAT);
      });

      it('should make a request to check the token', async () => {
        gitLabAPI.setClientInfo({ name: clientInfo.name, version: clientInfo.version });
        await gitLabAPI.checkToken(PAT);

        expect(fetch).toHaveBeenCalledWith(`${gitlabBaseUrl}/api/v4/personal_access_tokens/self`, {
          headers: {
            Authorization: `Bearer ${PAT}`,
            'User-Agent': `code-completions-language-server-experiment (${clientInfo.name}:${clientInfo?.version})`,
          },
          method: 'GET',
        });
      });

      it('should return correct message and reason when token is not active', async () => {
        (fetch as jest.Mock).mockResolvedValueOnce({
          ok: true,
          json: () => ({ active: false, scopes: ['read_user'] }),
        });

        const { reason, message } = await gitLabAPI.checkToken(PAT);

        expect(reason).toBe('not_active');
        expect(message).toBe('Token is not active.');
      });

      it('should return correct message and reason when token does not have enough scopes', async () => {
        (fetch as jest.Mock).mockResolvedValueOnce({
          ok: true,
          json: () => ({ active: true, scopes: ['read_api'] }),
        });

        const { reason, message } = await gitLabAPI.checkToken(PAT);

        expect(reason).toBe('invalid_scopes');
        expect(message).toBe(`Token has scope(s) 'read_api' (needs 'api').`);
      });

      it('should return correct message and reason when token check failed', async () => {
        (fetch as jest.Mock).mockRejectedValueOnce('Request failed.');

        const { reason, message } = await gitLabAPI.checkToken(PAT);

        expect(reason).toBe('unknown');
        expect(message).toBe('Failed to check token: Request failed.');
      });

      it('should return that token is valid when it is active and has correct scopes', async () => {
        (fetch as jest.Mock).mockResolvedValueOnce({
          ok: true,
          json: () => ({ active: true, scopes: ['api', 'read_user', 'read_api'] }),
        });

        const { reason, message, valid } = await gitLabAPI.checkToken(PAT);

        expect(valid).toBe(true);
        expect(reason).toBeUndefined();
        expect(message).toBeUndefined();
      });
    });

    describe('OAuth Token', () => {
      beforeEach(() => {
        gitLabAPI = new GitLabAPI(lsFetch, gitlabBaseUrl, token);
      });

      it('should make a request to check the token', async () => {
        gitLabAPI.setClientInfo({ name: clientInfo.name, version: clientInfo.version });
        await gitLabAPI.checkToken(token);

        expect(fetch).toHaveBeenCalledWith(`${gitlabBaseUrl}/oauth/token/info`, {
          headers: {
            Authorization: `Bearer ${token}`,
            'User-Agent': `code-completions-language-server-experiment (${clientInfo.name}:${clientInfo?.version})`,
          },
          method: 'GET',
        });
      });

      it('should return correct message and reason when token does not have enough scopes', async () => {
        (fetch as jest.Mock).mockResolvedValueOnce({
          ok: true,
          json: () => ({ scope: ['read_user'] }),
        });

        const { reason, message } = await gitLabAPI.checkToken(token);

        expect(reason).toBe('invalid_scopes');
        expect(message).toBe(`Token has scope(s) 'read_user' (needs 'api').`);
      });

      it('should return correct message and reason when token check failed', async () => {
        (fetch as jest.Mock).mockRejectedValueOnce('Request failed.');

        const { reason, message } = await gitLabAPI.checkToken(token);

        expect(reason).toBe('unknown');
        expect(message).toBe('Failed to check token: Request failed.');
      });

      it('should return that token is valid when it has correct scopes', async () => {
        (fetch as jest.Mock).mockResolvedValueOnce({
          ok: true,
          json: () => ({ scope: ['api', 'read_user', 'read_api'] }),
        });

        const { reason, message, valid } = await gitLabAPI.checkToken(token);

        expect(valid).toBe(true);
        expect(reason).toBeUndefined();
        expect(message).toBeUndefined();
      });
    });
  });
});
