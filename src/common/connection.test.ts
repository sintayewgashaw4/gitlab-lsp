import { setup } from './connection';
import { Connection, InlineCompletionRequest } from 'vscode-languageserver';
import { DocumentTransformer } from './documents';
import { FetchBase } from './fetch';
import { ConfigProvider } from '.';
import { createFakePartial } from './test_utils/create_fake_partial';
import { TELEMETRY_NOTIFICATION } from './tracking/snowplow_tracker';

// FIXME: The snowplow emitter keeps restarting itself in a loop
// https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/blob/bfc2bbfcee88309ffc03e93e3cbb97d345d8e5e1/src/common/tracking/snowplow/emitter.ts#L50-54
// this prevents the tests from finishing unless we mock out Snowplow
jest.mock('./tracking/snowplow/snowplow', () => ({
  Snowplow: {
    getInstance: jest.fn().mockReturnValue({
      trackStructEvent: jest.fn(),
      stop: jest.fn(),
      destroy: jest.fn(),
    }),
  },
}));

afterEach(() => {
  jest.clearAllMocks();
});

describe('Connection', () => {
  let mockConnection: Connection;

  beforeEach(() => {
    mockConnection = createFakePartial<Connection>({
      onInitialize: jest.fn(),
      onCompletion: jest.fn(),
      onCompletionResolve: jest.fn(),
      onRequest: jest.fn(),
      onDidChangeConfiguration: jest.fn(),
      onNotification: jest.fn(),
      onShutdown: jest.fn(),
    });
  });

  it('registers MessageHandler', () => {
    setup(mockConnection, {} as DocumentTransformer, {} as FetchBase, new ConfigProvider());

    expect(mockConnection.onInitialize).toHaveBeenCalled();
    expect(mockConnection.onCompletion).toHaveBeenCalled();
    expect(mockConnection.onCompletionResolve).toHaveBeenCalled();
    expect(mockConnection.onRequest).toHaveBeenCalledWith(
      InlineCompletionRequest.type,
      expect.any(Function),
    );
    expect(mockConnection.onDidChangeConfiguration).toHaveBeenCalled();
    expect(mockConnection.onNotification).toHaveBeenCalledWith(
      TELEMETRY_NOTIFICATION,
      expect.any(Function),
    );
    expect(mockConnection.onNotification).toHaveBeenCalledWith(
      'workspace/didChangeWorkspaceFolders',
      expect.any(Function),
    );
    expect(mockConnection.onShutdown).toHaveBeenCalled();
  });
});
