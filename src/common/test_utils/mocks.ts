import { CodeSuggestionResponse, CompletionToken } from '../api';
import { IDocContext } from '..';
import { TextDocumentIdentifier, TextDocumentPositionParams } from 'vscode-languageserver';
import { Position } from 'vscode-languageserver-textdocument';
import { CustomInitializeParams } from '../message_handler';

export const FILE_INFO: IDocContext = {
  filename: 'example.ts',
  prefix: 'const x = 10;',
  suffix: 'console.log(x);',
};

export const CODE_SUGGESTIONS_RESPONSE: CodeSuggestionResponse = {
  choices: [{ text: 'choice1' }, { text: 'choice2' }],
  status: 200,
};

export const CODES_SUGGESTIONS_COMPLETION_TOKEN: CompletionToken = {
  access_token: 'competion_token',
  expires_in: 3600,
  created_at: 1592227302,
};

export const INITIALIZE_PARAMS: CustomInitializeParams = {
  clientInfo: {
    name: 'Visual Studio Code',
    version: '1.82.0',
  },
  capabilities: { textDocument: { completion: {}, inlineCompletion: {} } },
  rootUri: '/',
  initializationOptions: {},
  processId: 1,
};

export const EMPTY_COMPLETION_CONTEXT: IDocContext = {
  prefix: '',
  suffix: '',
  filename: 'test.js',
};

export const SHORT_COMPLETION_CONTEXT: IDocContext = {
  prefix: 'abc',
  suffix: 'def',
  filename: 'test.js',
};

export const LONG_COMPLETION_CONTEXT: IDocContext = {
  prefix: 'abc 123',
  suffix: 'def 456',
  filename: 'test.js',
};

const textDocument: TextDocumentIdentifier = { uri: '/' };
const position: Position = { line: 0, character: 0 };

export const COMPLETION_PARAMS: TextDocumentPositionParams = { textDocument, position };
