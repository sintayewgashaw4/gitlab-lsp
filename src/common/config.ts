import { InitializeParams, WorkspaceFolder } from 'vscode-languageserver';
import { DEFAULT_TRACKING_ENDPOINT, ITelemetryOptions } from './tracking/snowplow_tracker';
import { GITLAB_API_BASE_URL } from './api';

export type IClientInfo = InitializeParams['clientInfo'];

export interface ICodeCompletionConfig {
  enableSecretRedaction?: boolean;
}

export interface IConfig {
  /** GitLab API URL used for getting code suggestions */
  baseUrl?: string;
  /** PAT or OAuth token used to authenticate to GitLab API */
  token?: string;
  clientInfo?: IClientInfo;
  codeCompletion?: ICodeCompletionConfig;
  telemetry?: ITelemetryOptions;
  workspaceFolders?: WorkspaceFolder[] | null;
}
/**
 * Config provider manages user configuration (e.g. baseUrl) and application state (e.g. codeCompletion.enabled)
 * TODO: Maybe in the future we would like to separate these two
 */
export class ConfigProvider {
  #config: IConfig;

  constructor() {
    this.#config = {
      baseUrl: GITLAB_API_BASE_URL,
      codeCompletion: {
        enableSecretRedaction: true,
      },
      telemetry: {
        enabled: true,
        trackingUrl: DEFAULT_TRACKING_ENDPOINT,
      },
    };
  }

  get(): IConfig {
    return this.#config;
  }
  /**
   * set sets the **top-level** property of the config
   * the new value completely overrides the old one
   */
  set<K extends keyof IConfig>(key: K, value: IConfig[K]) {
    this.#config[key] = value;
  }

  /**
   * merge adds `newConfig` properties into existing config, if the
   * property is present in both old and new config, `newConfig`
   * properties take precedence.
   *
   * This method replaces whole top-level properties. **It doesn't
   * perform deep merge.**
   *
   */
  merge(newConfig: Partial<IConfig>) {
    this.#config = { ...this.#config, ...newConfig };
  }
}
