import {
  CancellationToken,
  CompletionItem,
  CompletionItemKind,
  CompletionParams,
  Connection,
  Disposable,
  InitializeParams,
  InitializeResult,
  InlineCompletionItem,
  InlineCompletionList,
  InlineCompletionParams,
  Position,
  Range,
  TextDocumentIdentifier,
  TextDocumentSyncKind,
  WorkspaceFoldersChangeEvent,
} from 'vscode-languageserver';
import { CodeSuggestionResponseChoice, IGitLabAPI } from './api';
import { ConfigProvider, IConfig } from './config';
import {
  CODE_SUGGESTIONS_CATEGORY,
  CodeSuggestionRequestRejectionReason,
  IClientContext,
  SnowplowTracker,
  TRACKING_EVENTS,
} from './tracking/snowplow_tracker';
import { DocumentTransformer } from './documents';
import { isAtOrNearEndOfLine } from './suggestion/suggestion_filter';
import {
  API_ERROR_NOTIFICATION,
  API_RECOVERY_NOTIFICATION,
  CircuitBreaker,
} from './circuit_breaker/circuit_breaker';

export type ChangeConfigOptions = { settings: IConfig };

export type CustomInitializeParams = InitializeParams & {
  initializationOptions?: IClientContext;
};

export const TOKEN_CHECK_NOTIFICATION = '$/gitlab/token/check';

export const SUGGESTION_ACCEPTED_COMMAND = 'gitlab.ls.codeSuggestionAccepted';

export const DEBOUNCE_INTERVAL_MS = 300;

export interface MessageHandlerOptions {
  configProvider: ConfigProvider;
  api: IGitLabAPI;
  tracker: SnowplowTracker;
  connection: Connection;
  documentTransformer: DocumentTransformer;
}

export interface ITelemetryNotificationParams {
  category: 'code_suggestions';
  action: TRACKING_EVENTS;
  context: {
    trackingId: string;
  };
}

const waitMs = (msToWait: number) => new Promise((resolve) => setTimeout(resolve, msToWait));

export class MessageHandler {
  #configProvider: ConfigProvider;

  #api: IGitLabAPI;

  #tracker: SnowplowTracker;

  #connection: Connection;

  #documents: DocumentTransformer;

  #circuitBreaker = new CircuitBreaker();

  #subscriptions: Disposable[] = [];

  constructor({
    configProvider,
    api,
    tracker,
    connection,
    documentTransformer,
  }: MessageHandlerOptions) {
    this.#configProvider = configProvider;
    this.#api = api;
    this.#tracker = tracker;
    this.#connection = connection;
    this.#documents = documentTransformer;

    this.#subscribeToCircuitBreakerEvents();
  }

  onInitializeHandler = (params: CustomInitializeParams): InitializeResult => {
    const { clientInfo, initializationOptions, workspaceFolders } = params;

    this.#configProvider.set('clientInfo', clientInfo);
    this.#configProvider.set('workspaceFolders', workspaceFolders);

    this.#api.setClientInfo(clientInfo);

    this.#tracker.setClientContext({
      ide: initializationOptions?.ide ?? clientInfo,
      extension: initializationOptions?.extension,
    });

    return {
      capabilities: {
        completionProvider: {
          resolveProvider: true,
        },
        inlineCompletionProvider: true,
        textDocumentSync: TextDocumentSyncKind.Full,
      },
    };
  };

  didChangeConfigurationHandler = async (
    { settings }: ChangeConfigOptions = { settings: {} },
  ): Promise<void> => {
    const config = this.#configProvider.get();
    const telemetry = { ...config.telemetry, ...settings.telemetry };
    const codeCompletion = { ...config.codeCompletion, ...settings.codeCompletion };
    this.#configProvider.merge({ ...settings, telemetry, codeCompletion });

    await this.#api.configureApi(this.#configProvider.get());

    const { valid, reason, message } = await this.#api.checkToken(this.#configProvider.get().token);
    if (!valid) {
      this.#configProvider.set('token', undefined);

      await this.#connection.sendNotification(TOKEN_CHECK_NOTIFICATION, {
        message,
        reason,
      });
    }

    // Configure Snowplow tracking
    await this.#tracker.reconfigure({
      baseUrl: this.#configProvider.get().baseUrl,
      enabled: telemetry?.enabled,
      trackingUrl: telemetry?.trackingUrl,
      actions: telemetry?.actions,
    });
  };

  telemetryNotificationHandler = async ({
    category,
    action,
    context,
  }: ITelemetryNotificationParams) => {
    if (category === CODE_SUGGESTIONS_CATEGORY) {
      const { trackingId } = context;

      if (trackingId && this.#tracker.canClientTrackEvent(action)) {
        switch (action) {
          case TRACKING_EVENTS.ACCEPTED:
            this.#tracker.updateSuggestionState(trackingId, TRACKING_EVENTS.ACCEPTED);
            break;
          case TRACKING_EVENTS.REJECTED:
            this.#tracker.updateSuggestionState(trackingId, TRACKING_EVENTS.REJECTED);
            break;
          case TRACKING_EVENTS.CANCELLED:
            this.#tracker.updateSuggestionState(trackingId, TRACKING_EVENTS.CANCELLED);
            break;
          case TRACKING_EVENTS.SHOWN:
            this.#tracker.updateSuggestionState(trackingId, TRACKING_EVENTS.SHOWN);
            break;
          case TRACKING_EVENTS.NOT_PROVIDED:
            this.#tracker.updateSuggestionState(trackingId, TRACKING_EVENTS.NOT_PROVIDED);
            break;
          default:
            break;
        }
      }
    }
  };

  completionHandler = async (
    { textDocument, position }: CompletionParams,
    token: CancellationToken,
  ): Promise<CompletionItem[]> => {
    let codeSuggestions: CompletionItem[] = [];
    const uniqueTrackingId = SnowplowTracker.uniqueTrackingId();

    try {
      codeSuggestions = (
        await this.#getSuggestions(uniqueTrackingId, textDocument, position, token)
      ).map((choice, index) => ({
        label: `GitLab Suggestion ${index + 1}: ${choice.text}`,
        kind: CompletionItemKind.Text,
        insertText: choice.text,
        command: {
          title: 'Accept suggestion',
          command: SUGGESTION_ACCEPTED_COMMAND,
          arguments: [uniqueTrackingId],
        },
        data: {
          index,
          trackingId: uniqueTrackingId,
        },
      }));
    } catch (e) {
      this.#tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.ERRORED);
      this.#circuitBreaker.error();
      console.error('Failed to get code suggestions!', e);
    }

    return codeSuggestions;
  };

  inlineCompletionHandler = async (
    params: InlineCompletionParams,
    token: CancellationToken,
  ): Promise<InlineCompletionList> => {
    const uniqueTrackingId: string = SnowplowTracker.uniqueTrackingId();

    /**
     * Do not request suggestions if there is completion context attached
     * to the request. This condition will be removed once the suggestions
     * model supports contextual information coming from the intellisense
     * dropdown.
     *
     * See https://gitlab.com/gitlab-org/gitlab/-/issues/427841
     */
    if (params.context.selectedCompletionInfo) {
      await this.#tracker.trackRejectedSuggestionRequest(
        CodeSuggestionRequestRejectionReason.UnchangedDocument,
      );
      return { items: [] };
    }

    try {
      const items: InlineCompletionItem[] = (
        await this.#getSuggestions(uniqueTrackingId, params.textDocument, params.position, token)
      )?.map((choice) => ({
        insertText: choice.text,
        range: Range.create(params.position, params.position),
        command: {
          title: 'Accept suggestion',
          command: SUGGESTION_ACCEPTED_COMMAND,
          arguments: [uniqueTrackingId],
        },
        data: {
          trackingId: uniqueTrackingId,
        },
      }));
      return { items };
    } catch (e) {
      this.#tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.ERRORED);
      this.#circuitBreaker.error();
      console.error('Failed to get code suggestions!', e);
      return { items: [] };
    }
  };

  async #getSuggestions(
    uniqueTrackingId: string,
    textDocument: TextDocumentIdentifier,
    position: Position,
    token: CancellationToken,
  ): Promise<CodeSuggestionResponseChoice[]> {
    // debounce
    await waitMs(DEBOUNCE_INTERVAL_MS);
    if (token.isCancellationRequested) {
      return [];
    }

    console.log('Suggestion requested.');

    if (this.#circuitBreaker.isOpen()) {
      console.warn('Code suggestions were not requested as the circuit breaker is open.');
      return [];
    }

    const context = this.#documents.getContext(
      textDocument.uri,
      position,
      this.#configProvider.get().workspaceFolders ?? [],
    );

    if (!this.#configProvider.get().token) {
      return [];
    }

    if (context === undefined) {
      return [];
    }

    // Do not send a suggestion if content is less than 10 characters
    const contentLength = context?.prefix?.length + context?.suffix?.length;
    if (contentLength < 10) {
      return [];
    }

    if (!isAtOrNearEndOfLine(context.suffix)) {
      return [];
    }

    // Creates the suggestion and tracks suggestion_requested
    this.#tracker.setCodeSuggestionsContext(
      uniqueTrackingId,
      context,
      this.#configProvider.get().clientInfo,
    );

    const suggestionsResponse = await this.#api.getCodeSuggestions(context);

    this.#tracker.updateCodeSuggestionsContext(
      uniqueTrackingId,
      suggestionsResponse?.model,
      suggestionsResponse?.status,
    );

    if (suggestionsResponse?.error) {
      throw suggestionsResponse.error;
    }
    this.#tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.LOADED);

    this.#circuitBreaker.close();

    if (token.isCancellationRequested) {
      this.#tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.CANCELLED);
      return [];
    }

    if (
      !suggestionsResponse?.choices?.length ||
      suggestionsResponse?.choices.every(({ text }) => !text?.length)
    ) {
      this.#tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.NOT_PROVIDED);
      return [];
    }

    if (!this.#tracker.canClientTrackEvent(TRACKING_EVENTS.SHOWN)) {
      /* If the Client can detect when the suggestion is shown in the IDE, it will notify the Server.
            Otherwise the server will assume that returned suggestions are shown and tracks the event */
      this.#tracker.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.SHOWN);
    }
    return suggestionsResponse?.choices || [];
  }

  didChangeWorkspaceFoldersHandler = (params: WorkspaceFoldersChangeEvent) => {
    const { added, removed } = params;

    const removedKeys = removed.map(({ name }) => name);

    const currentWorkspaceFolders = this.#configProvider.get().workspaceFolders || [];
    const afterRemoved = currentWorkspaceFolders.filter((f) => !removedKeys.includes(f.name));
    const afterAdded = [...afterRemoved, ...(added || [])];

    this.#configProvider.set('workspaceFolders', afterAdded);
  };

  onShutdownHandler = () => {
    this.#subscriptions.forEach((subscription) => subscription?.dispose());
  };

  #subscribeToCircuitBreakerEvents() {
    this.#subscriptions.push(
      this.#circuitBreaker.onOpen(() => this.#connection.sendNotification(API_ERROR_NOTIFICATION)),
    );
    this.#subscriptions.push(
      this.#circuitBreaker.onClose(() =>
        this.#connection.sendNotification(API_RECOVERY_NOTIFICATION),
      ),
    );
  }
}
