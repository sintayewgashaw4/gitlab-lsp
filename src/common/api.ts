import { compare as semverCompare } from 'semver';

import { IClientInfo } from './config';
import { IDocContext } from './documents';
import { ICodeSuggestionModel } from './tracking/snowplow_tracker';
import { IFetch } from './fetch';

export const GITLAB_API_BASE_URL = 'https://gitlab.com';

export interface IGitLabAPI {
  configureApi({ token, baseUrl }: { token?: string; baseUrl?: string }): Promise<void>;
  checkToken(token: string | undefined): Promise<TokenCheckResponse>;

  setClientInfo(clientInfo: IClientInfo | undefined): void;

  getCodeSuggestions(file_info: IDocContext): Promise<CodeSuggestionResponse | undefined>;
}

export interface CodeSuggestionRequest {
  prompt_version: number;
  project_path: string;
  project_id: number;
  current_file: CodeSuggestionRequestCurrentFile;
}

export interface CodeSuggestionRequestCurrentFile {
  file_name: string;
  content_above_cursor: string;
  content_below_cursor: string;
}

export interface CodeSuggestionResponse {
  choices?: CodeSuggestionResponseChoice[];
  model?: ICodeSuggestionModel;
  status: number;
  error?: string;
}

export interface CodeSuggestionResponseChoice {
  text: string;
}

export interface CompletionToken {
  access_token: string;
  /* expires in number of seconds since `created_at` */
  expires_in: number;
  /* unix timestamp of the datetime of token creation */
  created_at: number;
}

export interface PersonalAccessToken {
  name: string;
  scopes: string[];
  active: boolean;
}

export interface OAuthToken {
  scope: string[];
}

export interface TokenCheckResponse {
  valid: boolean;
  reason?: 'unknown' | 'not_active' | 'invalid_scopes';
  message?: string;
}

export class GitLabAPI implements IGitLabAPI {
  #completionToken: CompletionToken | undefined;
  #token: string | undefined;
  #baseURL: string;
  #clientInfo: IClientInfo | undefined;
  #useNewApiVersion: boolean = false;
  #lsFetch: IFetch;

  constructor(lsFetch: IFetch, baseURL = GITLAB_API_BASE_URL, token?: string) {
    this.#token = token;
    this.#baseURL = baseURL;
    this.#lsFetch = lsFetch;
  }

  async configureApi({
    token,
    baseUrl = GITLAB_API_BASE_URL,
  }: {
    token?: string;
    baseUrl?: string;
  }) {
    this.#token = token;
    this.#baseURL = baseUrl;

    await this.#setCodeSuggestionsApiVersion();
  }

  #isPatToken(token: string): boolean {
    const regex = new RegExp(/^glpat-[0-9a-zA-Z-_]{20}$/);
    return regex.test(token);
  }

  async #checkPatToken(token: string): Promise<TokenCheckResponse> {
    const headers = this.#getDefaultHeaders(token);

    const response = await this.#lsFetch.get(
      `${this.#baseURL}/api/v4/personal_access_tokens/self`,
      { headers: headers },
    );

    if (response.ok) {
      const { active, scopes } = (await response.json()) as PersonalAccessToken;

      if (!active) {
        return {
          valid: false,
          reason: 'not_active',
          message: 'Token is not active.',
        };
      }

      if (!this.#hasValidScopes(scopes)) {
        const joinedScopes = scopes.map((scope) => `'${scope}'`).join(', ');

        return {
          valid: false,
          reason: 'invalid_scopes',
          message: `Token has scope(s) ${joinedScopes} (needs 'api').`,
        };
      }

      return { valid: true };
    } else {
      throw new Error(response.statusText);
    }
  }

  async #checkOAuthToken(token: string): Promise<TokenCheckResponse> {
    const headers = this.#getDefaultHeaders(token);

    const response = await this.#lsFetch.get(`${this.#baseURL}/oauth/token/info`, {
      headers,
    });

    if (response.ok) {
      const { scope: scopes } = (await response.json()) as OAuthToken;
      if (!this.#hasValidScopes(scopes)) {
        const joinedScopes = scopes.map((scope) => `'${scope}'`).join(', ');

        return {
          valid: false,
          reason: 'invalid_scopes',
          message: `Token has scope(s) ${joinedScopes} (needs 'api').`,
        };
      }

      return { valid: true };
    } else {
      throw new Error(response.statusText);
    }
  }

  async checkToken(token: string = ''): Promise<TokenCheckResponse> {
    try {
      if (this.#isPatToken(token)) {
        return await this.#checkPatToken(token);
      } else {
        return await this.#checkOAuthToken(token);
      }
    } catch (err) {
      return {
        valid: false,
        reason: 'unknown',
        message: `Failed to check token: ${err}`,
      };
    }
  }

  #hasValidScopes(scopes: string[]): boolean {
    return scopes.includes('api');
  }
  setClientInfo(clientInfo: IClientInfo) {
    this.#clientInfo = clientInfo;
  }
  async #getGitLabInstanceVersion(): Promise<string> {
    if (!this.#token) {
      throw 'Token needs to be provided';
    }

    const headers = this.#getDefaultHeaders(this.#token);

    const response = await this.#lsFetch.get(`${this.#baseURL}/api/v4/version`, {
      headers,
    });
    const { version } = await response.json();

    return version;
  }

  async #setCodeSuggestionsApiVersion() {
    if (this.#baseURL?.endsWith(GITLAB_API_BASE_URL)) {
      this.#useNewApiVersion = true;
      return;
    }

    try {
      const version = await this.#getGitLabInstanceVersion();
      this.#useNewApiVersion = semverCompare(version, 'v16.3.0') >= 0;
    } catch (err) {
      console.error(`Failed to get GitLab version: ${err}. Defaulting to the old API`);
    }
  }

  async #getCodeSuggestionsV1(requestData: CodeSuggestionRequest): Promise<CodeSuggestionResponse> {
    const completionToken = await this.#getCompletionToken();

    const headers = {
      ...this.#getDefaultHeaders(completionToken),
      'Content-Type': 'application/json',
      'X-Gitlab-Authentication-Type': 'oidc',
    };

    const response = await this.#lsFetch.post('https://codesuggestions.gitlab.com/v2/completions', {
      headers,
      body: JSON.stringify(requestData),
    });

    const data = await response.json();

    return { ...data, status: response.status };
  }

  async #getCodeSuggestionsV2(requestData: CodeSuggestionRequest): Promise<CodeSuggestionResponse> {
    if (!this.#token) {
      throw 'Token needs to be provided';
    }

    const headers = {
      ...this.#getDefaultHeaders(this.#token),
      'Content-Type': 'application/json',
    };

    const response = await this.#lsFetch.post(
      `${this.#baseURL}/api/v4/code_suggestions/completions`,
      { headers, body: JSON.stringify(requestData) },
    );

    const data = await response.json();
    return { ...data, status: response.status };
  }

  async getCodeSuggestions(context: IDocContext): Promise<CodeSuggestionResponse | undefined> {
    const request: CodeSuggestionRequest = {
      prompt_version: 1,
      project_path: '',
      project_id: -1,
      current_file: {
        content_above_cursor: context.prefix,
        content_below_cursor: context.suffix,
        file_name: context.filename,
      },
    };

    if (this.#useNewApiVersion) {
      return this.#getCodeSuggestionsV2(request);
    }
    return this.#getCodeSuggestionsV1(request);
  }

  async #getCompletionToken(): Promise<string> {
    // Check if token has expired
    if (this.#completionToken) {
      const unixTimestampNow = Math.floor(new Date().getTime() / 1000);
      if (unixTimestampNow < this.#completionToken.created_at + this.#completionToken.expires_in) {
        return this.#completionToken.access_token;
      }
    }

    if (!this.#token) {
      throw 'Token needs to be provided';
    }

    const headers = this.#getDefaultHeaders(this.#token);

    const response = await this.#lsFetch.post(`${this.#baseURL}/api/v4/code_suggestions/tokens`, {
      headers,
    });

    this.#completionToken = (await response.json()) as CompletionToken;
    return this.#completionToken.access_token;
  }

  #getDefaultHeaders(token: string) {
    return {
      Authorization: `Bearer ${token}`,
      'User-Agent': `code-completions-language-server-experiment (${this.#clientInfo?.name}:${this
        .#clientInfo?.version})`,
    };
  }
}
