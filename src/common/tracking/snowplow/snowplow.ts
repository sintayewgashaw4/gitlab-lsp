import {
  Payload,
  PayloadBuilder,
  SelfDescribingJson,
  StructuredEvent,
  buildStructEvent,
  trackerCore,
  TrackerCore,
} from '@snowplow/tracker-core';
import { v4 as uuidv4 } from 'uuid';
import { Emitter } from './emitter';
import { IFetch } from '../../fetch';

export type EnabledCallback = () => boolean;

export type SnowplowOptions = {
  appId: string;
  endpoint: string;
  timeInterval: number;
  maxItems: number;
  enabled: EnabledCallback;
};

/**
 * Adds the 'stm' parameter with the current time to the payload
 * Stringify all payload values
 * @param payload - The payload which will be mutated
 */
function preparePayload(payload: Payload): Record<string, string> {
  const stringifiedPayload: Record<string, string> = {};

  Object.keys(payload).forEach((key) => {
    stringifiedPayload[key] = String(payload[key]);
  });

  stringifiedPayload.stm = new Date().getTime().toString();

  return stringifiedPayload;
}

export class Snowplow {
  /** Disable sending events when it's not possible. */
  public disabled: boolean = false;
  private emitter: Emitter;
  private lsFetch: IFetch;
  private options: SnowplowOptions;
  private tracker: TrackerCore;

  // eslint-disable-next-line no-use-before-define
  private static instance?: Snowplow;

  private constructor(lsFetch: IFetch, options: SnowplowOptions) {
    this.lsFetch = lsFetch;
    this.options = options;
    this.emitter = new Emitter(
      this.options.timeInterval,
      this.options.maxItems,
      this.sendEvent.bind(this),
    );
    this.emitter.start();
    this.tracker = trackerCore({ callback: this.emitter.add.bind(this.emitter) });
  }

  public static getInstance(lsFetch: IFetch, options?: SnowplowOptions): Snowplow {
    if (!this.instance) {
      if (!options) {
        throw new Error('Snowplow should be instantiated');
      }
      const sp = new Snowplow(lsFetch, options);
      Snowplow.instance = sp;
    }

    // FIXME: this eslint violation was introduced before we set up linting
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return Snowplow.instance!;
  }

  private async sendEvent(events: PayloadBuilder[]): Promise<void> {
    if (!this.options.enabled() || this.disabled) {
      return;
    }

    try {
      const url = `${this.options.endpoint}/com.snowplowanalytics.snowplow/tp2`;
      const data = {
        schema: 'iglu:com.snowplowanalytics.snowplow/payload_data/jsonschema/1-0-4',
        data: events.map((event) => {
          const eventId = uuidv4();
          // All values prefilled below are part of snowplow tracker protocol
          // https://docs.snowplow.io/docs/collecting-data/collecting-from-own-applications/snowplow-tracker-protocol/#common-parameters
          // Values are set according to either common GitLab standard:
          // tna - representing tracker namespace and being set across GitLab to "gl"
          // tv - represents tracker value, to make it aligned with downstream system it has to be prefixed with "js-*""
          // aid - represents app Id is configured via options to gitlab_ide_extension
          // eid - represents uuid for each emitted event
          event.add('eid', eventId);
          event.add('p', 'app');
          event.add('tv', 'js-gitlab');
          event.add('tna', 'gl');
          event.add('aid', this.options.appId);

          return preparePayload(event.build());
        }),
      };
      const config = {
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data),
      };
      const response = await this.lsFetch.post(url, config);

      if (response.status !== 200) {
        console.warn(
          `Could not send telemetry to snowplow, this warning can be safely ignored. status=${response.status}`,
        );
      }
    } catch (error) {
      let errorHandled = false;

      if (typeof error === 'object' && 'errno' in (error as object)) {
        const errObject = error as object;

        // ENOTFOUND occurs when the snowplow hostname cannot be resolved.
        if ('errno' in errObject && errObject.errno == 'ENOTFOUND') {
          this.disabled = true;
          errorHandled = true;

          console.log('Disabling telemetry, unable to resolve endpoint address.');
        } else {
          console.warn('%j', errObject);
        }
      }

      if (!errorHandled) {
        console.warn('Failed to send telemetry event, this warning can be safely ignored', error);
      }
    }
  }

  public async trackStructEvent(
    event: StructuredEvent,
    context?: SelfDescribingJson[] | null,
  ): Promise<void> {
    this.tracker.track(buildStructEvent(event), context);
  }

  async stop() {
    await this.emitter.stop();
  }

  public destroy() {
    Snowplow.instance = undefined;
  }
}
