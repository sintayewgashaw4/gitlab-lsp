import fetch from 'cross-fetch';
import { Snowplow } from './snowplow';
import { IFetch, FetchBase } from '../../fetch';

jest.mock('cross-fetch');

class LsFetchMocked extends FetchBase implements IFetch {}

const structEvent = {
  category: 'test',
  action: 'test',
  label: 'test',
  value: 1,
};

const enabledMock = jest.fn();

const lsFetch = new LsFetchMocked();

const sp = Snowplow.getInstance(lsFetch, {
  appId: 'test',
  timeInterval: 1000,
  maxItems: 1,
  endpoint: 'http://localhost',
  enabled: enabledMock,
});

describe('Snowplow', () => {
  afterEach(() => {
    (fetch as jest.Mock).mockClear();
  });

  describe('Snowplow interface', () => {
    it('should initialize', async () => {
      const newSP = Snowplow.getInstance(lsFetch);
      expect(newSP).toBe(sp);
    });

    it('should let you track events', async () => {
      (fetch as jest.MockedFunction<typeof fetch>).mockResolvedValue({
        status: 200,
        statusText: 'OK',
      } as Response);
      enabledMock.mockReturnValue(true);
      await sp.trackStructEvent(structEvent);
      await sp.stop();
    });

    it('should let you stop when the program ends', async () => {
      enabledMock.mockReturnValue(true);
      await sp.stop();
    });

    it('should destroy the instance', async () => {
      const sameInstance = Snowplow.getInstance(lsFetch, {
        appId: 'test',
        timeInterval: 1000,
        maxItems: 1,
        endpoint: 'http://localhost',
        enabled: enabledMock,
      });
      expect(sp).toEqual(sameInstance);
      await sp.stop();
      sp.destroy();
      const newInstance = Snowplow.getInstance(lsFetch, {
        appId: 'test',
        timeInterval: 1000,
        maxItems: 1,
        endpoint: 'http://localhost',
        enabled: enabledMock,
      });
      expect(sp).not.toEqual(newInstance);
      await newInstance.stop();
    });
  });

  describe('should track and send events to snowplow', () => {
    beforeEach(() => {
      (fetch as jest.MockedFunction<typeof fetch>).mockClear();
      enabledMock.mockReturnValue(true);
    });

    afterEach(async () => {
      await sp.stop();
    });

    it('should send the events to snowplow', async () => {
      (fetch as jest.MockedFunction<typeof fetch>).mockClear();
      (fetch as jest.MockedFunction<typeof fetch>).mockResolvedValue({
        status: 200,
        statusText: 'OK',
      } as Response);

      const structEvent1 = { ...structEvent };
      structEvent1.action = 'action';

      await sp.trackStructEvent(structEvent);
      await sp.trackStructEvent(structEvent1);
      expect(fetch).toBeCalledTimes(2);
      await sp.stop();
    });
  });

  describe('enabled function', () => {
    it('should not send events to snowplow when disabled', async () => {
      (fetch as jest.MockedFunction<typeof fetch>).mockResolvedValue({
        status: 200,
        statusText: 'OK',
      } as Response);

      enabledMock.mockReturnValue(false);

      const structEvent1 = { ...structEvent };
      structEvent1.action = 'action';

      await sp.trackStructEvent(structEvent);
      expect(fetch).not.toBeCalled();
      await sp.stop();
    });

    it('should send events to snowplow when enabled', async () => {
      (fetch as jest.MockedFunction<typeof fetch>).mockResolvedValue({
        status: 200,
        statusText: 'OK',
      } as Response);

      enabledMock.mockReturnValue(true);

      const structEvent1 = { ...structEvent };
      structEvent1.action = 'action';

      await sp.trackStructEvent(structEvent);
      expect(fetch).toBeCalled();
      await sp.stop();
    });
  });

  describe('when hostname cannot be resolved', () => {
    it('should disable sending events', async () => {
      const logSpy = jest.spyOn(global.console, 'log');

      // FIXME: this eslint violation was introduced before we set up linting
      // eslint-disable-next-line @typescript-eslint/no-shadow
      const sp = Snowplow.getInstance(lsFetch, {
        appId: 'test',
        timeInterval: 1000,
        maxItems: 2,
        endpoint: 'http://askdalksdjalksjd.com',
        enabled: enabledMock,
      });

      (fetch as jest.MockedFunction<typeof fetch>).mockRejectedValue({
        message: '',
        errno: 'ENOTFOUND',
      });

      expect(sp.disabled).toBe(false);

      await sp.trackStructEvent(structEvent);
      await sp.stop();

      expect(sp.disabled).toBe(true);
      expect(logSpy).toHaveBeenCalledWith(
        'Disabling telemetry, unable to resolve endpoint address.',
      );

      logSpy.mockClear();
      await sp.trackStructEvent(structEvent);
      await sp.stop();

      expect(sp.disabled).toBe(true);
      expect(logSpy).not.toHaveBeenCalledWith();
    });
  });
});
