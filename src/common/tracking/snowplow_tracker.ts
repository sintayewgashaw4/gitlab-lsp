import { v4 as uuidv4 } from 'uuid';
import Ajv from 'ajv-draft-04';
import { SelfDescribingJson, StructuredEvent } from '@snowplow/tracker-core';
import { Snowplow } from './snowplow/snowplow';
import { IClientInfo } from '../config';
import { IFetch } from '../fetch';
import { IDocContext } from '../documents';
import * as CodeSuggestionContextSchema from './schemas/code_suggestion_context-2-1-0.json';
import * as IdeExtensionContextSchema from './schemas/ide_extension_version-1-1-0.json';
import * as SnowplowMetaSchema from './schemas/snowplow_schema-1-0-0.json';
import { version as lsVersion } from './ls_info.json';

/*
Custom notifications need to be sent from Client to Server to notify about the event:
suggestion_accepted, suggestion_rejected, suggestion_cancelled

The server knows when event happens:
suggestion_request_rejected, suggestion_requested, suggestion_loaded,
suggestion_not_provided, suggestion_error, suggestion_shown

---------------------------------------------------------------------------------
***************Suggestion tracking*******************************************
suggestion_request_rejected: Sent when the suggestion request is invalid
suggestion_requested: Sent when the IDE extension requests a suggestion
suggestion_loaded: Sent when the suggestion request came back
suggestion_not_provided: Sent when the suggestion request came back empty
suggestion_error: Sent when the suggestion request leads to an error
suggestion_shown: Sent when the suggestion is started to be shown to the user
suggestion_accepted: Sent when the suggestion was accepted
suggestion_rejected: Sent when the suggestion was rejected
suggestion_cancelled: Sent when the suggestion request was canceled
---------------------------------------------------------------------------------
 */
export enum TRACKING_EVENTS {
  REQUEST_REJECTED = 'suggestion_request_rejected',
  REQUESTED = 'suggestion_requested',
  LOADED = 'suggestion_loaded',
  NOT_PROVIDED = 'suggestion_not_provided',
  SHOWN = 'suggestion_shown',
  ERRORED = 'suggestion_error',
  ACCEPTED = 'suggestion_accepted',
  REJECTED = 'suggestion_rejected',
  CANCELLED = 'suggestion_cancelled',
}

const END_STATE_NO_TRANSITIONS_ALLOWED: TRACKING_EVENTS[] = [];

const stateGraph = new Map<TRACKING_EVENTS, TRACKING_EVENTS[]>([
  [TRACKING_EVENTS.REQUESTED, [TRACKING_EVENTS.LOADED, TRACKING_EVENTS.ERRORED]],
  [
    TRACKING_EVENTS.LOADED,
    [TRACKING_EVENTS.SHOWN, TRACKING_EVENTS.CANCELLED, TRACKING_EVENTS.NOT_PROVIDED],
  ],
  [TRACKING_EVENTS.SHOWN, [TRACKING_EVENTS.ACCEPTED, TRACKING_EVENTS.REJECTED]],
  [TRACKING_EVENTS.ERRORED, END_STATE_NO_TRANSITIONS_ALLOWED],
  [TRACKING_EVENTS.CANCELLED, END_STATE_NO_TRANSITIONS_ALLOWED],
  [TRACKING_EVENTS.NOT_PROVIDED, END_STATE_NO_TRANSITIONS_ALLOWED],
  [TRACKING_EVENTS.REJECTED, END_STATE_NO_TRANSITIONS_ALLOWED],
  [TRACKING_EVENTS.ACCEPTED, END_STATE_NO_TRANSITIONS_ALLOWED],
]);

const endStates = [...stateGraph]
  .filter(([, allowedTransitions]) => allowedTransitions.length === 0)
  .map(([state]) => state);

const SAAS_INSTANCE_URL = 'https://gitlab.com';
export const TELEMETRY_NOTIFICATION = '$/gitlab/telemetry';
export const CODE_SUGGESTIONS_CATEGORY = 'code_suggestions';

const GC_TIME = 60000;

export interface ITelemetryOptions {
  enabled?: boolean;
  baseUrl?: string;
  trackingUrl?: string;
  actions?: Array<{ action: TRACKING_EVENTS }>;
}

export interface IIDEInfo {
  name: string;
  version: string;
  vendor: string;
}

export interface IClientContext {
  ide?: IIDEInfo;
  extension?: IClientInfo;
}

export enum GitlabRealm {
  saas = 'saas',
  selfManaged = 'self-managed',
}

export interface ISnowplowCodeSuggestionContext {
  schema: string;
  data: {
    prefix_length?: number;
    suffix_length?: number;
    language?: string | null;
    user_agent?: string | null;
    gitlab_realm?: GitlabRealm;
    model_engine?: string | null;
    model_name?: string | null;
    api_status_code?: number | null;
  };
}

export interface ICodeSuggestionModel {
  lang: string;
  engine: string;
  name: string;
}

interface ISnowplowClientContext {
  schema: string;
  data: {
    ide_name?: string | null;
    ide_vendor?: string | null;
    ide_version?: string | null;
    extension_name?: string | null;
    extension_version?: string | null;
    language_server_version?: string | null;
  };
}

export const DEFAULT_TRACKING_ENDPOINT = 'https://snowplow.trx.gitlab.net';

const DEFAULT_SNOWPLOW_OPTIONS = {
  appId: 'gitlab_ide_extension',
  timeInterval: 5000,
  maxItems: 10,
};

export const EVENT_VALIDATION_ERROR_MSG = `Telemetry event context is not valid  - event won't be tracked.`;
export const TELEMETRY_DISABLED_WARNING_MSG =
  'GitLab Duo Code Suggestions telemetry is disabled. Please consider enabling telemetry to help improve our service.';

export enum CodeSuggestionRequestRejectionReason {
  UnchangedDocument = 'unchanged_document',
}
export class SnowplowTracker {
  private snowplow: Snowplow;
  #ajv = new Ajv({ strict: false });
  private options: ITelemetryOptions = {
    enabled: true,
    baseUrl: SAAS_INSTANCE_URL,
    trackingUrl: DEFAULT_TRACKING_ENDPOINT,
    // the list of events that the client can track themselves
    actions: [],
  };
  private clientContext: ISnowplowClientContext = {
    schema: 'iglu:com.gitlab/ide_extension_version/jsonschema/1-1-0',
    data: {},
  };
  private lsFetch: IFetch;

  private gitlabRealm: GitlabRealm = GitlabRealm.saas;
  private codeSuggestionsContextMap = new Map<string, ISnowplowCodeSuggestionContext>();
  #codeSuggestionStates = new Map<string, TRACKING_EVENTS>();

  constructor(lsFetch: IFetch, { trackingUrl = DEFAULT_TRACKING_ENDPOINT } = {}) {
    this.lsFetch = lsFetch;
    this.snowplow = Snowplow.getInstance(this.lsFetch, {
      ...DEFAULT_SNOWPLOW_OPTIONS,
      endpoint: trackingUrl,
      enabled: this.isEnabled.bind(this),
    });

    this.options.trackingUrl = trackingUrl;
    this.#ajv.addMetaSchema(SnowplowMetaSchema);
  }

  public async reconfigure({ baseUrl, enabled, trackingUrl, actions }: ITelemetryOptions) {
    if (typeof enabled !== 'undefined') {
      this.options.enabled = enabled;

      if (this.options.enabled === false) {
        console.warn(TELEMETRY_DISABLED_WARNING_MSG);
      }
    }

    if (baseUrl) {
      this.options.baseUrl = baseUrl;
      this.gitlabRealm = baseUrl.endsWith(SAAS_INSTANCE_URL)
        ? GitlabRealm.saas
        : GitlabRealm.selfManaged;
    }

    if (trackingUrl && this.options.trackingUrl !== trackingUrl) {
      await this.snowplow.stop();
      this.snowplow.destroy();
      this.snowplow = Snowplow.getInstance(this.lsFetch, {
        ...DEFAULT_SNOWPLOW_OPTIONS,
        endpoint: trackingUrl,
        enabled: this.isEnabled.bind(this),
      });
    }

    if (actions) {
      this.options.actions = actions;
    }
  }

  public isEnabled(): boolean {
    return Boolean(this.options.enabled);
  }

  public setClientContext(context: IClientContext) {
    this.clientContext.data = {
      ide_name: context?.ide?.name ?? null,
      ide_vendor: context?.ide?.vendor ?? null,
      ide_version: context?.ide?.version ?? null,
      extension_name: context?.extension?.name ?? null,
      extension_version: context?.extension?.version ?? null,
      language_server_version: lsVersion ?? null,
    };
  }

  public setCodeSuggestionsContext(
    uniqueTrackingId: string,
    suggestion: IDocContext | undefined,
    extension: IClientInfo,
  ) {
    console.log(`Telemetry: Received request to create a new suggestion`);

    // Only auto-reject if client is set up to track accepted and not rejected events.
    if (
      this.canClientTrackEvent(TRACKING_EVENTS.ACCEPTED) &&
      !this.canClientTrackEvent(TRACKING_EVENTS.REJECTED)
    ) {
      this.rejectOpenedSuggestions();
    }

    setTimeout(() => {
      if (this.codeSuggestionsContextMap.has(uniqueTrackingId)) {
        this.codeSuggestionsContextMap.delete(uniqueTrackingId);
        this.#codeSuggestionStates.delete(uniqueTrackingId);
      }
    }, GC_TIME);

    this.codeSuggestionsContextMap.set(uniqueTrackingId, {
      schema: 'iglu:com.gitlab/code_suggestions_context/jsonschema/2-1-0',
      data: {
        suffix_length: suggestion?.suffix.length ?? 0,
        prefix_length: suggestion?.prefix.length ?? 0,
        gitlab_realm: this.gitlabRealm,
        language: null,
        user_agent: `code-completions-language-server-experiment (${extension?.name}:${extension?.version})`, // same as User-Agent header we sent to code suggestions server
      },
    });
    this.#codeSuggestionStates.set(uniqueTrackingId, TRACKING_EVENTS.REQUESTED);

    this.trackCodeSuggestionsEvent(TRACKING_EVENTS.REQUESTED, uniqueTrackingId).catch((e) =>
      console.warn('could not track telemetry', e),
    );

    console.log(`Telemetry: New suggestion ${uniqueTrackingId} has been requested`);
  }

  public updateCodeSuggestionsContext(
    uniqueTrackingId: string,
    model: ICodeSuggestionModel | undefined,
    status: number | undefined,
  ) {
    const context = this.codeSuggestionsContextMap.get(uniqueTrackingId);

    if (context) {
      context.data.language = model?.lang ?? null;
      context.data.model_engine = model?.engine ?? null;
      context.data.model_name = model?.name ?? null;
      context.data.api_status_code = status ?? null;

      this.codeSuggestionsContextMap.set(uniqueTrackingId, context);
    }
  }

  public canClientTrackEvent(event: TRACKING_EVENTS) {
    return this.options.actions?.some(({ action }) => action === event);
  }

  private async trackCodeSuggestionsEvent(eventType: TRACKING_EVENTS, data: string) {
    if (!this.isEnabled()) {
      return;
    }

    const event: StructuredEvent = {
      category: CODE_SUGGESTIONS_CATEGORY,
      action: eventType,
      label: data,
    };

    try {
      const contexts: SelfDescribingJson[] = [this.clientContext];
      const codeSuggestionContext = this.codeSuggestionsContextMap.get(data);

      if (codeSuggestionContext) {
        contexts.push(codeSuggestionContext);
      }

      const suggestionContextValid = this.#ajv.validate(
        CodeSuggestionContextSchema,
        codeSuggestionContext?.data,
      );
      if (!suggestionContextValid) {
        console.warn(EVENT_VALIDATION_ERROR_MSG);
        console.log(this.#ajv.errors);
        return;
      }
      const ideExtensionContextValid = this.#ajv.validate(
        IdeExtensionContextSchema,
        this.clientContext?.data,
      );
      if (!ideExtensionContextValid) {
        console.warn(EVENT_VALIDATION_ERROR_MSG);
        console.log(this.#ajv.errors);
        return;
      }

      await this.snowplow.trackStructEvent(event, contexts);
    } catch (error) {
      console.log(`Failed to track telemetry event: ${eventType}`, error);
    }
  }

  public rejectOpenedSuggestions() {
    console.log(`Telemetry: Reject all opened suggestions`);

    this.#codeSuggestionStates.forEach((state, uniqueTrackingId) => {
      if (endStates.includes(state)) {
        return;
      }

      this.updateSuggestionState(uniqueTrackingId, TRACKING_EVENTS.REJECTED);
    });
  }

  public trackRejectedSuggestionRequest(
    reason: CodeSuggestionRequestRejectionReason,
  ): Promise<void> {
    console.log(`Telemetry: Suggestion request rejected. Reason "${reason}"`);
    const event: StructuredEvent = {
      category: CODE_SUGGESTIONS_CATEGORY,
      action: TRACKING_EVENTS.REQUEST_REJECTED,
      label: reason,
    };
    return this.snowplow.trackStructEvent(event, [this.clientContext]);
  }

  public updateSuggestionState(uniqueTrackingId: string, newState: TRACKING_EVENTS): void {
    const state = this.#codeSuggestionStates.get(uniqueTrackingId);
    if (!state) {
      console.log(`Telemetry: The suggestion with ${uniqueTrackingId} can't be found`);
      return;
    }

    const allowedTransitions = stateGraph.get(state);
    if (!allowedTransitions) {
      console.log(
        `Telemetry: The suggestion's ${uniqueTrackingId} state ${state} can't be found in state graph`,
      );
      return;
    }

    if (!allowedTransitions.includes(newState)) {
      console.log(
        `Telemetry: Unexpected transition from ${state} into ${newState} for ${uniqueTrackingId}`,
      );
      // Allow state to update to ACCEPTED despite 'allowed transitions' constraint.
      if (newState !== TRACKING_EVENTS.ACCEPTED) {
        return;
      }

      console.log(
        `Telemetry: Conditionally allowing transition to accepted state for ${uniqueTrackingId}`,
      );
    }
    this.#codeSuggestionStates.set(uniqueTrackingId, newState);

    this.trackCodeSuggestionsEvent(newState, uniqueTrackingId).catch((e) =>
      console.warn('could not track telemetry', e),
    );

    console.log(`Telemetry: ${uniqueTrackingId} transisted from ${state} to ${newState}`);
  }

  static uniqueTrackingId(): string {
    return uuidv4();
  }
}
