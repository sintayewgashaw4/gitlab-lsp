import { getDocContext } from '.';

describe('getDocContext', () => {
  const document = {
    languageId: 'javascript',
    uri: 'file:///Users/test/projects/status-page/scripts/sync_emojis.js',
    version: 42,
    positionAt: jest.fn(),
    getText: jest.fn().mockReturnValue('console.log("hello!")'),
    lineCount: 10,
    offsetAt: jest.fn(),
  };

  const position = {
    line: 18,
    character: 2,
  };

  const workspaceFolders = [
    {
      uri: 'file:///Users/test/projects/LSP',
      name: 'LSP',
    },
    {
      name: 'status-page-private',
      uri: 'file:///Users/test/projects/status-page',
      index: 2,
    },
  ];

  it('should set the relative path to the file based on Workspace Folders', () => {
    expect(getDocContext(document, position, workspaceFolders).filename).toBe(
      'scripts/sync_emojis.js',
    );
  });
});
