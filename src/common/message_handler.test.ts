import {
  CancellationToken,
  CompletionItem,
  CompletionParams,
  Connection,
  InlineCompletionList,
  InlineCompletionParams,
  InlineCompletionTriggerKind,
  Position,
  Range,
  SelectedCompletionInfo,
  WorkspaceFolder,
} from 'vscode-languageserver';
import { ConfigProvider, IConfig } from './config';
import {
  DEBOUNCE_INTERVAL_MS,
  MessageHandler,
  SUGGESTION_ACCEPTED_COMMAND,
} from './message_handler';
import { createFakePartial } from './test_utils/create_fake_partial';
import { IGitLabAPI } from './api';
import {
  CODE_SUGGESTIONS_CATEGORY,
  CodeSuggestionRequestRejectionReason,
  SnowplowTracker,
  TRACKING_EVENTS,
} from './tracking/snowplow_tracker';
import { DocumentTransformer } from './documents';
import {
  COMPLETION_PARAMS,
  LONG_COMPLETION_CONTEXT,
  SHORT_COMPLETION_CONTEXT,
} from './test_utils/mocks';
import * as completionFilters from './suggestion/suggestion_filter';
import { CIRCUIT_BREAK_INTERVAL_MS } from './circuit_breaker/circuit_breaker';

jest.useFakeTimers();

describe('MessageHandler', () => {
  let messageHandler: MessageHandler;

  const tracker: SnowplowTracker = createFakePartial<SnowplowTracker>({
    canClientTrackEvent: jest.fn(),
    updateSuggestionState: jest.fn(),
    setCodeSuggestionsContext: jest.fn(),
    updateCodeSuggestionsContext: jest.fn(),
    reconfigure: jest.fn(),
    trackRejectedSuggestionRequest: jest.fn(),
  });

  afterEach(() => {
    (Object.keys(tracker) as Array<keyof SnowplowTracker>).forEach((mockTrackingMethod) => {
      (tracker[mockTrackingMethod] as jest.Mock).mockReset();
    });
  });

  describe('didChangeConfigurationHandler', () => {
    let configProvider: ConfigProvider;
    let api: IGitLabAPI;

    beforeEach(async () => {
      configProvider = new ConfigProvider();
      api = createFakePartial<IGitLabAPI>({
        configureApi: jest.fn(),
        checkToken: jest.fn(),
      });
      jest.mocked(api.checkToken).mockResolvedValue({ valid: true });
      messageHandler = new MessageHandler({
        connection: createFakePartial<Connection>({}),
        api,
        tracker,
        configProvider,
        documentTransformer: {} as DocumentTransformer,
      });
    });

    it('can change all settings', async () => {
      const expectedConfig: IConfig = {
        baseUrl: 'https://test.url',
        codeCompletion: {
          enableSecretRedaction: false,
        },
        telemetry: {
          enabled: false,
          trackingUrl: 'https://telemetry.url',
        },
      };
      await messageHandler.didChangeConfigurationHandler({
        settings: expectedConfig,
      });

      expect(configProvider.get()).toEqual(expectedConfig);
    });

    it('uses the preexisting settings if the new config does not have them', async () => {
      const defaultConfig = new ConfigProvider().get();

      await messageHandler.didChangeConfigurationHandler({
        settings: {},
      });

      expect(configProvider.get()).toEqual(defaultConfig);
    });

    it('handles partial updaes of nested properties', async () => {
      const defaultConfig = new ConfigProvider().get();

      await messageHandler.didChangeConfigurationHandler({
        settings: {
          telemetry: {
            enabled: false,
          },
          codeCompletion: {
            enableSecretRedaction: false,
          },
        },
      });

      expect(configProvider.get().telemetry).toEqual({
        ...defaultConfig.telemetry,
        enabled: false,
      });
      expect(configProvider.get().codeCompletion).toEqual({
        ...defaultConfig.codeCompletion,
        enableSecretRedaction: false,
      });
    });
  });

  describe('telemetryNotificationHandler', () => {
    beforeEach(() => {
      const configProvider = createFakePartial<ConfigProvider>({});
      const api = createFakePartial<IGitLabAPI>({});

      messageHandler = new MessageHandler({
        connection: createFakePartial<Connection>({}),
        api,
        tracker,
        configProvider,
        documentTransformer: {} as DocumentTransformer,
      });
    });
    const trackingId = 'uniqueId';
    describe.each([
      [TRACKING_EVENTS.ACCEPTED],
      [TRACKING_EVENTS.REJECTED],
      [TRACKING_EVENTS.CANCELLED],
      [TRACKING_EVENTS.SHOWN],
      [TRACKING_EVENTS.NOT_PROVIDED],
    ])('%s event', (trackingEvent: TRACKING_EVENTS) => {
      it('should be tracked when client can notify about it', async () => {
        (tracker.canClientTrackEvent as jest.Mock).mockReturnValue(true);

        await messageHandler.telemetryNotificationHandler({
          category: CODE_SUGGESTIONS_CATEGORY,
          action: trackingEvent,
          context: {
            trackingId,
          },
        });

        expect(tracker.updateSuggestionState).toHaveBeenCalledWith(trackingId, trackingEvent);
      });

      it('should NOT be tracked when client cannot notify about it', () => {
        (tracker.canClientTrackEvent as jest.Mock).mockReturnValue(false);
        expect(
          messageHandler.telemetryNotificationHandler({
            category: CODE_SUGGESTIONS_CATEGORY,
            action: trackingEvent,
            context: {
              trackingId,
            },
          }),
        );
        expect(tracker.updateSuggestionState).not.toHaveBeenCalledWith(trackingId, trackingEvent);
      });
    });
  });

  describe('completionHandler && inlineCompletionHandler', () => {
    const isAtOrNearEndOfLineListener = jest.spyOn(completionFilters, 'isAtOrNearEndOfLine');
    isAtOrNearEndOfLineListener.mockReturnValue(true);
    const mockGetCodeSuggestions = jest.fn();

    let api = createFakePartial<IGitLabAPI>({
      getCodeSuggestions: mockGetCodeSuggestions,
    });

    afterEach(() => {
      mockGetCodeSuggestions.mockReset();
    });

    describe('completion', () => {
      const mockGetContext = jest.fn().mockReturnValue(LONG_COMPLETION_CONTEXT);
      let configProvider: ConfigProvider;
      let token: CancellationToken;

      const requestCompletionNoDebounce = (
        params: CompletionParams,
        tkn: CancellationToken,
      ): Promise<CompletionItem[]> => {
        const result = messageHandler.completionHandler(params, tkn);
        jest.advanceTimersByTime(DEBOUNCE_INTERVAL_MS);
        return result;
      };

      beforeEach(async () => {
        token = createFakePartial<CancellationToken>({ isCancellationRequested: false });
        configProvider = new ConfigProvider();
        configProvider.set('token', 'abc');
        api = createFakePartial<IGitLabAPI>({
          getCodeSuggestions: mockGetCodeSuggestions,
        });

        const mockedDocumentTransformer = {
          getContext: mockGetContext,
        } as unknown as DocumentTransformer;

        messageHandler = new MessageHandler({
          connection: createFakePartial<Connection>({
            sendNotification: jest.fn().mockResolvedValue({}),
          }),
          api,
          tracker,
          configProvider,
          documentTransformer: mockedDocumentTransformer,
        });
      });

      it('sends a suggestion when all setup is present', async () => {
        jest.spyOn(SnowplowTracker, 'uniqueTrackingId').mockReturnValue('unique tracking id');
        mockGetCodeSuggestions.mockReturnValueOnce({
          choices: [{ text: 'mock suggestion' }],
          model: {
            lang: 'js',
          },
        });

        const result = await requestCompletionNoDebounce(COMPLETION_PARAMS, token);
        expect(result.length).toEqual(1);
        const [suggestedItem] = result;
        expect(suggestedItem.insertText).toBe('mock suggestion');
        expect(suggestedItem.command).toEqual({
          title: expect.any(String),
          command: SUGGESTION_ACCEPTED_COMMAND,
          arguments: ['unique tracking id'],
        });
      });

      it('does not send a suggestion when context is short', async () => {
        mockGetContext.mockReturnValueOnce(SHORT_COMPLETION_CONTEXT);

        const result = await requestCompletionNoDebounce(COMPLETION_PARAMS, token);

        expect(result).toEqual([]);
      });

      it('does not send a suggestion when in middle of line', async () => {
        isAtOrNearEndOfLineListener.mockReturnValueOnce(false);

        const result = await requestCompletionNoDebounce(COMPLETION_PARAMS, token);

        expect(result).toEqual([]);
      });

      describe('Suggestions not provided', () => {
        it('should track suggestion not provided when no choices returned', async () => {
          mockGetCodeSuggestions.mockReturnValueOnce(() => ({
            choices: [],
            model: {
              lang: 'js',
            },
          }));

          const result = await requestCompletionNoDebounce(COMPLETION_PARAMS, token);
          expect(tracker.updateSuggestionState).toHaveBeenCalledWith(
            expect.any(String),
            TRACKING_EVENTS.NOT_PROVIDED,
          );
          expect(result).toEqual([]);
        });

        it('should track suggestion not provided when every choice is empty', async () => {
          mockGetCodeSuggestions.mockReturnValueOnce(() => ({
            choices: [{ text: '' }, { text: undefined }],
            model: {
              lang: 'js',
            },
          }));

          const result = await requestCompletionNoDebounce(COMPLETION_PARAMS, token);
          expect(tracker.updateSuggestionState).toHaveBeenCalledWith(
            expect.any(String),
            TRACKING_EVENTS.NOT_PROVIDED,
          );
          expect(result).toEqual([]);
        });
      });

      describe('Circuit breaking', () => {
        const getCompletions = async () => {
          const result = await requestCompletionNoDebounce(COMPLETION_PARAMS, token);
          return result;
        };
        const turnOnCircuitBreaker = async () => {
          await getCompletions();
          await getCompletions();
          await getCompletions();
          await getCompletions();
        };

        it('starts breaking after 4 errors', async () => {
          mockGetCodeSuggestions.mockResolvedValue({
            choices: [{ text: 'mock suggestion' }],
            model: {
              lang: 'js',
            },
          });
          const successResult = await getCompletions();
          expect(successResult.length).toEqual(1);
          mockGetCodeSuggestions.mockRejectedValue(new Error('test problem'));
          await turnOnCircuitBreaker();

          mockGetCodeSuggestions.mockReset();
          mockGetCodeSuggestions.mockResolvedValue({
            choices: [{ text: 'mock suggestion' }],
            model: {
              lang: 'js',
            },
          });

          const result = await getCompletions();
          expect(result).toEqual([]);
          expect(mockGetCodeSuggestions).not.toHaveBeenCalled();
        });

        it(`fetches completions again after circuit breaker's break time elapses`, async () => {
          jest.useFakeTimers().setSystemTime(new Date(Date.now()));

          mockGetCodeSuggestions.mockRejectedValue(new Error('test problem'));
          await turnOnCircuitBreaker();

          mockGetCodeSuggestions.mockReset();
          mockGetCodeSuggestions.mockResolvedValue({
            choices: [{ text: 'mock suggestion' }],
            model: {
              lang: 'js',
            },
          });
          jest.advanceTimersByTime(CIRCUIT_BREAK_INTERVAL_MS + 1);

          const result = await getCompletions();

          expect(result).toHaveLength(1);
          expect(mockGetCodeSuggestions).toHaveBeenCalled();
        });
      });
    });

    describe('inlineCompletion', () => {
      const mockGetContext = jest.fn().mockReturnValue(LONG_COMPLETION_CONTEXT);
      let configProvider: ConfigProvider;

      const inlineCompletionParams: InlineCompletionParams = {
        ...COMPLETION_PARAMS,
        position: Position.create(1, 1),
        context: {
          triggerKind: InlineCompletionTriggerKind.Invoked,
        },
      };

      const requestInlineCompletionNoDebounce = (
        params: InlineCompletionParams,
        tkn: CancellationToken,
      ): Promise<InlineCompletionList> => {
        const result = messageHandler.inlineCompletionHandler(params, tkn);
        jest.advanceTimersByTime(DEBOUNCE_INTERVAL_MS);
        return result;
      };
      let token: CancellationToken;

      beforeEach(async () => {
        token = createFakePartial<CancellationToken>({ isCancellationRequested: false });
        configProvider = new ConfigProvider();
        configProvider.set('token', 'abc');

        const mockedDocumentTransformer = {
          getContext: mockGetContext,
        } as unknown as DocumentTransformer;
        messageHandler = new MessageHandler({
          connection: createFakePartial<Connection>({
            sendNotification: jest.fn().mockResolvedValue({}),
          }),
          api,
          tracker,
          configProvider,
          documentTransformer: mockedDocumentTransformer,
        });
      });

      describe('when the inline completion requests contains contextual information', () => {
        let result: InlineCompletionList;

        beforeEach(async () => {
          result = await requestInlineCompletionNoDebounce(
            createFakePartial<InlineCompletionParams>({
              context: {
                triggerKind: InlineCompletionTriggerKind.Automatic,
                selectedCompletionInfo: createFakePartial<SelectedCompletionInfo>({ text: 'foo' }),
              },
            }),
            token,
          );
        });

        it('returns an empty completion list', () => {
          expect(result.items).toHaveLength(0);
        });

        it('tracks rejected request reason as "unchanged_document"', () => {
          expect(tracker.trackRejectedSuggestionRequest).toHaveBeenCalledWith(
            CodeSuggestionRequestRejectionReason.UnchangedDocument,
          );
        });
      });

      it('sends a suggestion when all setup is present', async () => {
        jest.spyOn(SnowplowTracker, 'uniqueTrackingId').mockReturnValue('unique tracking id');

        mockGetCodeSuggestions.mockReturnValueOnce({
          choices: [{ text: 'mock suggestion' }],
          model: {
            lang: 'js',
          },
        });

        const { items } = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);

        expect(items.length).toEqual(1);
        const [suggestedItem] = items;
        expect(suggestedItem.insertText).toBe('mock suggestion');
        expect(suggestedItem.command).toEqual({
          title: expect.any(String),
          command: SUGGESTION_ACCEPTED_COMMAND,
          arguments: ['unique tracking id'],
        });
        expect(suggestedItem.range).toEqual(
          Range.create(Position.create(1, 1), Position.create(1, 1)),
        );
      });

      it('does not send a suggestion when context is short', async () => {
        mockGetContext.mockReturnValueOnce(SHORT_COMPLETION_CONTEXT);

        const { items } = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);

        expect(items).toEqual([]);
      });

      it('does not send a suggestion when in middle of line', async () => {
        isAtOrNearEndOfLineListener.mockReturnValueOnce(false);

        const { items } = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);

        expect(items).toEqual([]);
      });

      it('tracks suggestion cancelled if the suggestion request has been cancelled before API responded', async () => {
        const testToken = { isCancellationRequested: false };

        mockGetCodeSuggestions.mockImplementation(async () => {
          // simulate that request has been cancelled before API responded
          testToken.isCancellationRequested = true;
          return {
            choices: [{ text: 'mock suggestion' }],
            model: {
              lang: 'js',
            },
          };
        });

        const { items } = await requestInlineCompletionNoDebounce(
          inlineCompletionParams,
          testToken as CancellationToken,
        );

        expect(items).toEqual([]);
        expect(tracker.updateSuggestionState).toHaveBeenCalledWith(
          expect.any(String),
          TRACKING_EVENTS.CANCELLED,
        );
      });

      describe('Suggestions not provided', () => {
        it('should track suggestion not provided when no choices returned', async () => {
          mockGetCodeSuggestions.mockReturnValueOnce(() => ({
            choices: [],
            model: {
              lang: 'js',
            },
          }));

          const result = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);
          expect(tracker.updateSuggestionState).toHaveBeenCalledWith(
            expect.any(String),
            TRACKING_EVENTS.NOT_PROVIDED,
          );
          expect(result.items).toEqual([]);
        });

        it('should track suggestion not provided when every choice is empty', async () => {
          mockGetCodeSuggestions.mockReturnValueOnce(() => ({
            choices: [{ text: '' }, { text: undefined }],
            model: {
              lang: 'js',
            },
          }));

          const result = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);
          expect(tracker.updateSuggestionState).toHaveBeenCalledWith(
            expect.any(String),
            TRACKING_EVENTS.NOT_PROVIDED,
          );
          expect(result.items).toEqual([]);
        });
      });

      describe('Circuit breaking', () => {
        const getCompletions = async () => {
          const result = await requestInlineCompletionNoDebounce(inlineCompletionParams, token);
          return result;
        };
        const turnOnCircuitBreaker = async () => {
          await getCompletions();
          await getCompletions();
          await getCompletions();
          await getCompletions();
        };

        it('starts breaking after 4 errors', async () => {
          mockGetCodeSuggestions.mockReset();
          mockGetCodeSuggestions.mockResolvedValue({
            choices: [{ text: 'mock suggestion' }],
            model: {
              lang: 'js',
            },
          });
          const successResult = await getCompletions();
          expect(successResult.items.length).toEqual(1);
          mockGetCodeSuggestions.mockRejectedValue(new Error('test problem'));
          await turnOnCircuitBreaker();

          mockGetCodeSuggestions.mockReset();
          mockGetCodeSuggestions.mockResolvedValue({
            choices: [{ text: 'mock suggestion' }],
            model: {
              lang: 'js',
            },
          });

          const result = await getCompletions();
          expect(result?.items).toEqual([]);
          expect(mockGetCodeSuggestions).not.toHaveBeenCalled();
        });

        it(`fetches completions again after circuit breaker's break time elapses`, async () => {
          jest.useFakeTimers().setSystemTime(new Date(Date.now()));

          mockGetCodeSuggestions.mockRejectedValue(new Error('test problem'));
          await turnOnCircuitBreaker();

          mockGetCodeSuggestions.mockReset();
          mockGetCodeSuggestions.mockResolvedValue({
            choices: [{ text: 'mock suggestion' }],
            model: {
              lang: 'js',
            },
          });
          jest.advanceTimersByTime(CIRCUIT_BREAK_INTERVAL_MS + 1);

          const result = await getCompletions();

          expect(result?.items).toHaveLength(1);
          expect(mockGetCodeSuggestions).toHaveBeenCalled();
        });
      });

      describe('debouncing', () => {
        beforeEach(() => {
          mockGetCodeSuggestions.mockResolvedValue({
            choices: [{ text: 'mock suggestion' }],
            model: {
              lang: 'js',
            },
          });
        });

        it('returns empty result if token was cancelled before debounce interval', async () => {
          const testToken = { isCancellationRequested: false };

          const completionPromise = messageHandler.inlineCompletionHandler(
            inlineCompletionParams,
            testToken as CancellationToken,
          );
          testToken.isCancellationRequested = true;
          jest.advanceTimersByTime(DEBOUNCE_INTERVAL_MS);

          const result = await completionPromise;
          expect(result.items).toEqual([]);
        });

        it('continues to call API if token has not been cancelled before debounce interval', async () => {
          const completionPromise = messageHandler.inlineCompletionHandler(
            inlineCompletionParams,
            token,
          );
          jest.advanceTimersByTime(DEBOUNCE_INTERVAL_MS);

          const result = await completionPromise;
          expect(result.items.length).toEqual(1);
        });
      });
    });
  });

  describe('didChangeWorkspaceFoldersHandler', () => {
    const wf1: WorkspaceFolder = { uri: '//Users/workspace/1', name: 'workspace1' };
    const wf2: WorkspaceFolder = { uri: '//Users/workspace/2', name: 'workspace2' };
    const wf3: WorkspaceFolder = { uri: '//Users/workspace/3', name: 'workspace3' };

    it('adds and removes folders', async () => {
      const configProvider = new ConfigProvider();
      const api = createFakePartial<IGitLabAPI>({});

      const documentTransformer = createFakePartial<DocumentTransformer>({});
      messageHandler = new MessageHandler({
        connection: createFakePartial<Connection>({}),
        api,
        tracker,
        configProvider,
        documentTransformer,
      });
      messageHandler.didChangeWorkspaceFoldersHandler({ added: [wf1, wf2], removed: [] });

      expect(configProvider.get().workspaceFolders).toHaveLength(2);
      expect(configProvider.get().workspaceFolders).toEqual([wf1, wf2]);

      messageHandler.didChangeWorkspaceFoldersHandler({ added: [wf3], removed: [wf2] });

      expect(configProvider.get().workspaceFolders).toHaveLength(2);
      expect(configProvider.get().workspaceFolders).toEqual([wf1, wf3]);
    });
  });
});
