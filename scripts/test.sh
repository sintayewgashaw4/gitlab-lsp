#!/bin/bash

set -ex

# Install HTTP proxy used by tests
apt-get update
apt-get install -y tinyproxy

npm ci
npm run compile
npm run bundle # tests that we can bundle the LS for browser and node
npm run test:ci
