## [3.18.1](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.18.0...v3.18.1) (2023-12-01)


### Bug Fixes

* the LS only needs api scope, read_user is redundant ([ea4a9ee](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/ea4a9ee71afedebd527ce4f9d3c7f573d5f3e042))



# [3.18.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.17.0...v3.18.0) (2023-11-30)


### Features

* Reject inline completions with intellisense context ([7726054](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/7726054f9831001b9d090c3bb055953a59bb3b3c))



# [3.17.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.16.1...v3.17.0) (2023-11-29)


### Bug Fixes

* inline completion items missing range ([259b2c8](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/259b2c895ff12a756404e1dcf677ab4f92bcafc5))


### Features

* add debouncing and cancellation to LS ([93a33c5](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/93a33c5903fb0de10a59e3ef1392da00cf32f112))
* **telemetry:** only autoreject if client sends accepted events ([9e0d8cf](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/9e0d8cf0aa911afd95a0399f8e2498af251c5ba6))



## [3.16.1](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.16.0...v3.16.1) (2023-11-24)

* Exports check token notification type for the VS Code Extension


# [3.16.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.15.0...v3.16.0) (2023-11-24)


### Bug Fixes

* Fix browser build ([f9f65d9](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/f9f65d9ab2231faa3d94b47641c3cddb8dca1ec1))
* remove hello notification ([015304d](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/015304db38bc85944889f73dee68fbe3899a7551))


### Features

* Add Circuit Breaker ([f933c55](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/f933c55adf9fe8946ffbc1a4e35b8378ef053825))
* Notify Clients about API error/recovery ([34027eb](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/34027ebe6b98489554503f252aa452e84095ea2f))
* remove unnecessary console log ([83f16be](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/83f16bea76d3cf4ae91979616405d7dd12a29c58))
* **telemetry:** Implement "suggestion rejected" logic ([5d7815f](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/5d7815f752e705b7cc12ca77b99aedaa8a0b8d3f))



# [3.15.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.14.0...v3.15.0) (2023-11-13)


### Bug Fixes

* Restore missing ajv dependency ([208a6ad](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/208a6ad24b38deb30a6a3d1f91491e99d241bcc7))


### Features

* Handle empty suggestion text on the LS side ([e1162f2](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/e1162f2f6cdd11422de7359ef7eeaf983dd849e7))



# [3.14.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.13.0...v3.14.0) (2023-11-10)


### Features

* add accept suggestion command ([a2b3e7c](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/commit/a2b3e7c2ab7919fb5a1977c06fea2b1210c761cf))



# [3.13.0](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.12.1...v3.13.0) (2023-11-09)

- Swap a node dependency that would prevent web contexts from using the bundle (!94)

## [3.12.1](https://gitlab.com/gitlab-org/editor-extensions/gitlab-language-server-for-code-suggestions/compare/v3.12.0...v3.12.1) (2023-11-06)

- No functional changes, only testing the new release process (!81)

## v3.12.0

- Allow clients to track `suggestion_not_provided` (!83)

## v3.11.0

- Track `language_server_version` with telemetry events (!78)

## v3.10.0

- Support `textDocument/inlineCompletion` message (!75)

## v3.9.0

- Only provide suggestions when cursor is at or near end of a line. (!61)
- Validate Snowplow events against the schema before tracking (!55)

## v3.8.0

- Fix issue where partial config updates used wrong values (!65) (also refactors LS configuration)

## v3.7.0

- Support using an HTTP proxy for connections (!35)
- Fix duplicated Snowplow events (!67)

## v3.6.0

- Bundle whole LS into one JS file to be used in VS Code (!62)

## v3.5.0

- Update Snowplow event `code_suggestions_context` schema to v2-1-0 (!64)
- Make Secret redaction respect the config setting, also enable it in the browser build (!57)

## v3.4.0

- Handle better error response from the Code Suggestions server (!56)

## v3.3.0

- Don't make suggestions for short content (!30)
- asdf `.tool-versions` file added for nodejs v18.16.0 (!47)

## v3.2.0

- Send all console messages to STDERR (!45)
- Disable snowplow events when host cannot be resolved (!45)
- Update `code_suggestions_context` schema to `v2-0-1` (!43)

## v3.1.0

- Add `suggestion_not_provided` telemetry event when suggestions are returned empty (!38)
- Allow Client to detect the `suggestion_shown` event (!38)

## v3.0.0

- Use custom `ide` and `extension` initialization parameters for telemetry (!32)

## v2.2.1

- Rely on `model.lang` value in the response from Code Suggestions server to set `language` property for the Code Suggestions telemetry context (!34)

## v2.2.0

- Enable Code Suggestions telemetry by default (!41)

## v2.1.0

- Send the relative file path to the Code suggestions server (!29)
- Update `appId` for Snowplow tracking (!36)

## v2.0.0

- Add Snowplow tracking library (!25)
- Add Code Suggestions telemetry (!27)
- Move all Client settings handling to the `DidChangeConfiguration` notification handler (!27)

## v1.0.0

- Update `token/check`  notification to `$/gitlab/token/check` (!17)
- Document required and optional messages for server & client (!17)
- Document initialize capabilities (!17)
- Check that `completion` is supported by the client (!17)

## v0.0.8

- Bumping version to keep packages in sync (!22)

## v0.0.7

- Revert `re2` usage as it was causing issues with some platforms (!20)

## v0.0.6

- Fix npm package publishing
- Refactor TS build to accommodate WebWorker LSP

## v0.0.5

- Start publishing an npm package

## v0.0.4

- Add new code suggestions endpoint (!12)
- Add token check (!13)
- Subscribe to document sync events and publish diagnostics (empty for now) (!15)
- Use `re2` to work with Gitleaks regex (!16)

## v0.0.3

- Documenting server startup configuration and capabilities (!8)
- Bug fix for the code suggestions (!8)

## v0.0.2

- Easier build and publish (!10)
- Refactor for browser entrypoint (!6)
- Add secrets redaction (!7)

## v0.0.1

- Base version
